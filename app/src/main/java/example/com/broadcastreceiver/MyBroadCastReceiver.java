package example.com.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MyBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        Bundle extras = intent.getExtras();
        final String tag = "Intent Intercepter";
        if (extras != null) {
            if (extras.containsKey("number")) {
                Object num = extras.get("number");
                if (num.toString().equals("0")) {
                    Intent i = new Intent();
                    i.setClassName("example.com.broadcastreceiver", "example.com.broadcastreceiver.SecondActivity");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                }
                else if (num.toString().equals("1")) {

                    try{
                    Intent i = new Intent("com.company.package.FOO");
                    intent.setClassName("example.com.broadcastreceiverattacker", "example.com.broadcastreceiverattacker.AttackActivity");
                    // i.addCategory(Intent.CATEGORY_LAUNCHER);
                    //i.setClassName("example.com.broadcastreceiverattacker", "example.com.broadcastreceiverattacker.AttackActivity");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    String data = intent.getStringExtra("number");
                    Log.i(tag, data);
                    //i.setComponent(new ComponentName("com.example.broadcastreceiverattacker", "com.company.package.FOO"));
                    context.startActivity(i);
                    }
                    catch (Exception e) {
                        Toast.makeText(context, "Intercepted", Toast.LENGTH_LONG).show();
                    }

                }
                // startActivity(i);

            }

               /* System.out.println("Value is:"+extras.get("number"));
                Object num = extras.get("number");
                System.out.println(num);

                String phoneNo = num.toString();
                String message = "Hi, Your Registration has been confirmed";*/
            // SmsManager smsManager = SmsManager.getDefault();
            // smsManager.sendTextMessage(phoneNo, null, message, null, null);
        }

    }
}